package ui.tests.kotlinTests

import base.TestsSpecUI
import base.Utils.randomInt
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import ui.pageObjects.AddRemoveElements

@ExtendWith(TestsSpecUI::class)
class AddRemoveElementsKotlin {

    private val page = AddRemoveElements()

    @Test
    fun `User should see correct page title`() {
        page
            .open()
            .hasPageTitle()

    }

    @Test
    fun `User can add any count of delete buttons`() {
        val items = randomInt(10)

        page.open()

        for (i in 1..items) {
            page
                .addElement()
                .hasDeleteButtonsCount(i)
        }
    }

    @Test
    fun `User can delete buttons`() {
        page
            .open()
            .addElementsJS(5)
            .hasDeleteButtonsCount(5)
            .delete(4)
            .hasDeleteButtonsCount(4)
            .delete(0)
            .hasDeleteButtonsCount(3)
            .delete(1)
            .hasDeleteButtonsCount(2)
    }
}