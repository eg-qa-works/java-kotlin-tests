package ui.tests.javaTests;

import base.TestsSpecUI;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import ui.pageObjects.AddRemoveElements;

import static base.Utils.randomInt;

@ExtendWith(TestsSpecUI.class)
public class AddRemoveElementsJava {

  //Java test could use kotlin classes as well
  AddRemoveElements page = new AddRemoveElements();

  @Test
  @DisplayName("User should see correct page title")
  public void test1() {
    page
        .open()
        .hasPageTitle();
  }

  @Test
  @DisplayName("User can add any count of delete buttons")
  public void test2() {
    int items = randomInt(10);

    page.open();

    for (int i = 1; i <= items; i++) {
      page
          .addElement()
          .hasDeleteButtonsCount(i);
    }
  }

  @Test
  @DisplayName("User can delete buttons")
  public void test3() {
    page
        .open()
        .addElementsJS(5)
        .hasDeleteButtonsCount(5)
        .delete(4)
        .hasDeleteButtonsCount(4)
        .delete(0)
        .hasDeleteButtonsCount(3)
        .delete(1)
        .hasDeleteButtonsCount(2);
  }
}
