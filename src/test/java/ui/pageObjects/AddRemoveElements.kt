package ui.pageObjects

import base.Constants
import com.codeborne.selenide.CollectionCondition.size
import com.codeborne.selenide.Condition.text
import com.codeborne.selenide.Selectors.byText
import com.codeborne.selenide.Selenide.*

class AddRemoveElements {

    private val content = `$`("#content")
    private val addElement = content.`$`(byText("Add Element"))

    fun open(): AddRemoveElements {
        open(Constants.site + "/add_remove_elements/")
        return this
    }

    fun addElement(): AddRemoveElements {
        addElement.click()
        return this
    }

    fun addElementsJS(count: Int): AddRemoveElements {
        repeat(count) { executeJavaScript<String>("addElement()") }
        return this
    }

    fun delete(position: Int): AddRemoveElements {
        content
            .`$`(byText("Delete"), position)
            .click()
        return this
    }

    fun hasPageTitle(): AddRemoveElements {
        content
            .`$`("h3")
            .shouldHave(text("Add/Remove Elements"))
        return this
    }

    fun hasDeleteButtonsCount(count: Int): AddRemoveElements {
        `$$`("#elements button").shouldHave(size(count))
        return this
    }
}