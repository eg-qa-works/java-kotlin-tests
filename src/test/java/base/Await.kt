package base

/**
 * Recursively tries to run an [function]
 *
 * @param tries Maximum number of attempts
 * @param millis Delay between attempts
 * @param function Function to run
 */
fun <T> await(tries: Int = 5, millis: Int = 500, function: () -> T): T {

    tailrec fun runFunction(tries: Int, millis: Int): T {
        return try {
            function()
        } catch (e: Throwable) {
            if (tries <= 1) {
                throw e
            } else {
                Thread.sleep(millis.toLong())
                runFunction(tries - 1, millis)
            }
        }
    }
    return runFunction(tries, millis)
}