package base;

import com.google.common.primitives.Chars;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Utils {
  public static final Gson gson = new Gson();

  public static int randomInt(int count) {
    return (int) ((Math.random() * (count - 1)) + 1);
  }

  @NotNull
  public static String randomPart(int length, String symbols) {
    return shuffle(symbols).substring(0, length);
  }

  @NotNull
  public static String randomPart(int length) {
    String symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz23456789";
    return randomPart(length, symbols);
  }

  @NotNull
  public static String randomPart() {
    return randomPart(6);
  }

  @NotNull
  public static String lowerPart() {
    Long minuend = 3000000000000L;
    return String.valueOf((minuend - new Date().getTime()));
  }

  public static String shuffle(@NotNull String s) {
    List<Character> chars = Chars.asList(s.toCharArray());
    Collections.shuffle(chars);
    return StringUtils.join(chars.stream().toArray());
  }
}
