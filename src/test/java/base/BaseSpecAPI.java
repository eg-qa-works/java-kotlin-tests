package base;

import apis.Api;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.jupiter.api.extension.Extension;

public class BaseSpecAPI implements Extension {
  public static final Api api = new Api();

  private BaseSpecAPI() {
    if (Boolean.valueOf(System.getProperty("enableRestAssuredLogger"))) {
      RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }
  }
}
