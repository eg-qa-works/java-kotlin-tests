package apis.test

import apis.src.petstore.pet.models.requests.Category
import apis.src.petstore.pet.models.requests.Pet
import apis.src.petstore.pet.models.requests.Tag
import base.BaseSpecAPI
import base.BaseSpecAPI.api
import base.Utils.randomPart
import base.await
import io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(BaseSpecAPI::class)
class TestPetsApi {

    @Test
    fun `can add pet to petstore`() {
        val petName = randomPart()

        api.pets.addRaw(
            Pet(
                name = petName,
                category = Category(),
                tags = listOf(Tag(), Tag()),
                status = "available",
                photoUrls = listOf("url")
            )
        )
            .then()
            .statusCode(200)
            .body("name", `is`(petName))
            .body(matchesJsonSchemaInClasspath("petstore/pet.json"))
    }

    @Test
    fun `can get pet from petstore`() {
        val petName = randomPart()

        val pet = api.pets.add(Pet(petName))

        await {
            api.pets.getRaw(pet.id)
                .then()
                .statusCode(200)
                .body("name", `is`(petName))
        }
    }
}