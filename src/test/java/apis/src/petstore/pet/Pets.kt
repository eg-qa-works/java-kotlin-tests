package apis.src.petstore.pet

import apis.src.petstore.pet.models.requests.Pet
import base.Constants.petstore
import base.Utils.gson
import io.restassured.RestAssured.given
import io.restassured.response.Response

class Pets {
    private val prefix = "$petstore/v2/pet"

    fun getRaw(petId: Long): Response {
        return given()
            .header("content-type", "application/json; charset=UTF-8")
            .`when`()
            .get("$prefix/$petId")
    }

    fun get(petId: Long): Pet {
        val request = getRaw(petId)

        request
            .then()
            .statusCode(200)

        return gson.fromJson(request.asString(), Pet::class.java)
    }

    fun addRaw(body: Pet = Pet()): Response {
        return given()
            .header("content-type", "application/json; charset=UTF-8")
            .body(gson.toJson(body))
            .`when`()
            .post(prefix)
    }

    fun add(body: Pet = Pet()): Pet {
        val request = addRaw(body)

        request
            .then()
            .statusCode(200)

        return gson.fromJson(request.asString(), Pet::class.java)
    }
}