package apis.src.petstore.pet.models.requests

import base.Utils.randomPart
import java.util.*

data class Pet(
    val name: String = randomPart(),
    val id: Long = Date().time,
    val category: Category? = null,
    val photoUrls: List<String>? = null,
    val tags: List<Tag>? = null,
    val status: String? = null
)

data class Category(
    val id: Long = 0,
    val name: String = randomPart()
)

data class Tag(
    val id: Long = 0,
    val name: String = randomPart()
)